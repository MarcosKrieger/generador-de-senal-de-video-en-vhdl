# Generador de señal de video en VHDL

Generador de video de 640x480 pixel con refresco de pantalla de 60 Hz. Mediante un pulsador, se selecciona la visualización de las barras en forma horizontal o vertical. La señal de 8 barras con los colores primarios generados con la señal de color RGB.

![](VGA.jpeg)


