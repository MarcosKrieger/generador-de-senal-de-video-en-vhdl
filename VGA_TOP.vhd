----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:28:50 05/23/2022 
-- Design Name: 
-- Module Name:    VGA_Top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_arith.all;   --Para poder usar el (-'1' o +'1')
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity VGA_Top is
 port (clk, reset 	: in std_logic; 
	   selec		: in std_logic; --Selecci�n
       hsync, vsync : out std_logic;   --bits de sincronismo horizontal y vertical
       RGB 			: out std_logic_vector (7 downto 0)  --Salida para pines R, G, y B
       );
end VGA_Top;

architecture Behavioral of VGA_Top is


	COMPONENT vga_sync
	PORT(
		clk 	 : IN std_logic;
		reset 	 : IN std_logic;          
		hsync 	 : OUT std_logic;
		vsync 	 : OUT std_logic;
		video_on : OUT std_logic;
		p_tick 	 : OUT std_logic;
		pixel_x  : OUT std_logic_vector(9 downto 0);
		pixel_y  : OUT std_logic_vector(9 downto 0)
		);
	END COMPONENT;


signal Svideo_on: std_logic ; --se�al para habilitar la asignacion de la imagen
signal Spixel_x, Spixel_y : std_logic_vector (9 downto 0);
--signal S_RGB, RGB_next: std_logic_vector (2 downto 0); 
	
begin

--------------------------------------------------------------
--						INSTACIACI�N VGA
--------------------------------------------------------------
	Inst_vga_sync: vga_sync PORT MAP(
		clk => clk,
		reset => reset,
        hsync=> hsync,
      	vsync=> vsync,
      	video_on=> Svideo_on,
		p_tick => open,
		pixel_x => Spixel_x,
		pixel_y => Spixel_y
	);

---------------------------------------------------------------
--Barras colores
---------------------------------------------------------------
process(clk, selec, Spixel_x, Spixel_y)
begin
	if(clk'event and clk ='1') then
		if(selec = '1') then
		  
		  --HORIZONTAL
			if(Spixel_x >= 0 and Spixel_x <= 80)
				then RGB <= "11111111";  --Blanco
			elsif(Spixel_x >= 81 and Spixel_x <= 160)
				then RGB <= "11111100"; --Amarillo
			elsif(Spixel_x >= 161 and Spixel_x <= 240)
				then RGB <= "00011111"; --Cyan
			elsif(Spixel_x >= 241 and Spixel_x <= 320)
				then RGB <= "00011100"; --Verde
			elsif(Spixel_x >= 321 and Spixel_x <= 400)
				then RGB <= "11100011"; --Magenta
			elsif(Spixel_x >= 401 and Spixel_x <= 480)
				then RGB <= "11100000"; --Rojo
			elsif(Spixel_x >= 481 and Spixel_x <= 560)
				then RGB <= "00000011"; --Azul
			elsif(Spixel_x >= 561 and Spixel_x <= 640)
				then RGB <= "00000000"; --Negro 
			end if;
			
			
			--VERTICAL
		else
			if(Spixel_y >= 0 and Spixel_y <= 60)
				then RGB <= "11111111";
			elsif(Spixel_y >= 61 and Spixel_y <= 120)
				then RGB <= "11111100";
			elsif(Spixel_y >= 121 and Spixel_y <= 180)
				then RGB <= "00011111";
			elsif(Spixel_y >= 181 and Spixel_y <= 240)
				then RGB <= "00011100";
			elsif(Spixel_y >= 301 and Spixel_y <= 300)
				then RGB <= "11100011";
			elsif(Spixel_y >= 361 and Spixel_y <= 360)
				then RGB <= "11100000";
			elsif(Spixel_y >= 421 and Spixel_y <= 420)
				then RGB <= "00000011";
			elsif(Spixel_y >= 481 and Spixel_y <= 480)
				then RGB <= "00000000";
			end if;
		end if;
	end if;
end process;

	 
end Behavioral;

