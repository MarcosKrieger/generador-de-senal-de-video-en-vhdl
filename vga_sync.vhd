----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:32:13 05/23/2022 
-- Design Name: 
-- Module Name:    Sync - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.std_logic_arith.all;   --Para poder usar el (-'1' o +'1')
library UNISIM;
use UNISIM.Vcomponents.ALL;


entity vga_sync is
    port (clk, reset		: in std_logic;
          hsync , vsync 	: out std_logic;
          video_on, p_tick	: out std_logic;
          pixel_x , pixel_y 	: out std_logic_vector (9 downto 0)
          );
end vga_sync;

architecture Behavioral of vga_sync is

-- 				640x480    @60Hz

constant HD: integer :=640; --�rea horizontal de la pantalla
constant HF: integer:=16;   --Borde Derecho horizontal.
constant HB: integer:=48;   --Borde Izquierdo horizontal.
constant HR: integer:=96;   --RETRACE regi�n en la que los haces de electrones vuelven al borde izquierdo. 
                            --La se�al de v�deo debe estar desactivada, y la longitud de esta regi�n es de 96 p�xeles.
									
constant VD : integer :=480; --�rea vertical de la pantalla
constant VF : integer:=10;   --Borde Inferior vertical.
constant VB: integer :=33;   --Borde Superior vertical
constant VR : integer :=2;   --Retrace Vertical

-- Contador Modulo 4
signal mod2_reg, mod2_next : std_logic;

--contadores de sincronismo
signal v_count_reg, v_count_next : unsigned(9 downto 0);
signal h_count_reg, h_count_next : unsigned (9 downto 0 );

--se�ales sincronismo
signal v_sync_reg, h_sync_reg   : std_logic;
signal v_sync_next, h_sync_next : std_logic;

--se�ales de estado
signal h_end, v_end, pixel_tick: std_logic; 

-- se�ales finales
begin
process (clk , reset)
begin
	if reset='1' then
		mod2_reg <= '0';
		v_count_reg <= (others => '0');
		h_count_reg <= (others => '0');
		v_sync_reg <= '0';
		h_sync_reg <= '0';
		
	elsif (clk'event and clk = '1') then
		mod2_reg <= mod2_next;
		v_count_reg <= v_count_next;
		h_count_reg <= h_count_next;
		v_sync_reg <= v_sync_next;
		h_sync_reg <= h_sync_next;
	end if;
end process;
	
-- modulo 2 para generar 25 MHz para el pixel_tick
mod2_next <= not mod2_reg;

-- 25 MHz pixel tick
pixel_tick <= '1' when mod2_reg= '1' else '0';

-- Final del contador horizontal
h_end <= '1' when h_count_reg = (HD + HF + HB + HR - 1) else '0'; --799 (p-1)

-- Final del contador vertical
v_end <= '1' when v_count_reg = (VD + VF + VB + VR - 1) else '0'; --524 (L-1)
        
-- Contador horizontal
process (h_count_reg, h_end, pixel_tick)
	begin
		if pixel_tick = '1' then -- 25 MHz tick 
			if h_end = '1' then
				h_count_next <= (others => '0');
        	else
				h_count_next <= h_count_reg + 1;
			end if;
		else
			h_count_next <= h_count_reg;
		end if;
end process;

-- Contador vertical
process (v_count_reg, h_end, v_end, pixel_tick)
begin
	if pixel_tick = '1' and h_end = '1' then
		if (v_end = '1') then
			v_count_next <= (others =>'0');
      else
			v_count_next <= v_count_reg + 1;
		end if;
	else    
		v_count_next <= v_count_reg;
	end if;
end process;


h_sync_next <= '1' when (h_count_reg >= (HD + HF)) --656
                   and (h_count_reg <= (HD + HF + HR - 1)) else '0'; --751 
					
					
v_sync_next <=
                '1' when (v_count_reg >= (VD + VF)) --490
                    and (v_count_reg <= (VD + VF + VR - 1)) else '0'; --491
				    
					 					 
-- video on/off
video_on <=
            '1' when (h_count_reg < HD) and (v_count_reg < VD) else
            '0';
				
-- Asignacion a las salidas
hsync <= h_sync_reg;
vsync <= v_sync_reg;
pixel_x <= std_logic_vector(h_count_reg);
pixel_y <= std_logic_vector(v_count_reg);
p_tick <= pixel_tick;

end Behavioral;